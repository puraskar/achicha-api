#!/usr/bin/env bash

#install required packages
sudo add-apt-repository -y ppa:ondrej/php5
sudo apt-get -y update
sudo apt-get -y install git
sudo apt-get -y install unzip
sudo apt-get -y install gcc
sudo apt-get -y install vim
sudo apt-get -y install apache2
sudo apt-get -y install php5 php5-common php5-cli php5-dev php5-mysql php5-pgsql php5-gd php5-mcrypt php5-curl php-pear libapache2-mod-php5 php5-xdebug

install mysql-server - version 5.5 - set root pass to root
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server

#configure vhost
sudo cp /var/www/achica-api/httpsdocs/provision/vagrant.achica.worldstores.co.uk.conf /etc/apache2/sites-available/
sudo mkdir /var/www/achica-api/logfiles
sudo mkdir /var/www/achica-api/logs && sudo chmod 777 /var/www/achica-api/logs

#install/enable mod rewrite & enable vhost
sudo a2enmod rewrite
sudo a2ensite vagrant.achica.worldstores.co.uk
sudo service apache2 restart

#install composer
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
sudo mv composer.phar /usr/bin/composer
