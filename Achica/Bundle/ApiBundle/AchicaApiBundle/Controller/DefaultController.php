<?php

namespace AchicaApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AchicaApiBundle:Default:index.html.twig');
    }
}
