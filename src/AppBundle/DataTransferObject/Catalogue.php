<?php

namespace AppBundle\DataTransferObject;

use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Type;
use AppBundle\DataTransferObject\DTOInterface;

/**
 * @XmlRoot("catalogue")
 */
class Catalogue implements DTOInterface
{
    /**
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @Type("integer")
     */
    private $clientId;


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Catalogue
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set clientId
     *
     * @param $clientId
     *
     * @return Catalogue
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return \int
     */
    public function getClientId()
    {
        return $this->clientId;
    }
}

