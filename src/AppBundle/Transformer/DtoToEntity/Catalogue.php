<?php

namespace AppBundle\Transformer\DtoToEntity;

use AppBundle\DataTransferObject\DTOInterface;
use AppBundle\Entity\EntityInterface;
use AppBundle\Entity\Catalogue as CatalogueEntity;

class Catalogue
{
    
    public function transform(DTOInterface $dto, $entity = null)
    {
        if (!$entity instanceof CatalogueEntity) {
            $entity = new CatalogueEntity();
        }
        $entity->setDate($dto->getDate())
               ->setClientId(3);
        return $entity;
    }
    
    public function reverse(EntityInterface $catalogue)
    {
        
    }
}
