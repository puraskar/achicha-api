<?php

namespace AppBundle\Transformer\DtoToEntity;


class DtoToEntityTransformerFactory
{
    
    public function __construct() 
    {
        ;
    }
    
    public function getTransformer($type)
    {
        switch ($type)  {
            case 'catalogue':
                return new Catalogue();
            default:
                throw new \Exception ('No Transformer Found');
            
        }
    }
}
