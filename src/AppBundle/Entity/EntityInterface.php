<?php

namespace AppBundle\Entity;


interface EntityInterface 
{
    /**
     * @return integer
     */
    public function getId();

}