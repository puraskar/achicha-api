<?php


namespace AppBundle\Exception;

class InvalidEntityException extends \RuntimeException
{

    public function __construct($message)
    {
        parent::__construct($message);
    }

}