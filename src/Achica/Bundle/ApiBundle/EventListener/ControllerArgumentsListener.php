<?php

namespace Achica\Bundle\ApiBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use JMS\Serializer\Serializer;
use JMS\SerializerBundle\Exception\XmlErrorException;
use AppBundle\DataTransferObject\Catalogue;


class ControllerArgumentsListener
{
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();

        // the controller is an attribute of the request
        $controller = $request->attributes->get('_controller');

        // controller looks like class::method, we need array(class, method)
        $controller = explode('::', $controller);
        
        /**
         * @ToDo check if the request type is post, check if dto is provided. etc. etc. 
         */

        //try {
            //new \ReflectionParameter($controller, 'dto');
        //}
        //catch (\ReflectionException $e) {
            // no controller argument named "comment" was found
            //return;
        //}

        if ($request->attributes->has('dto')) {
            // comment attribute already exists, do not overwrite it
            return;
        }
        
        
        $type = $request->query->get('type');

        try
        {
            if ($type) {
                // deserialize the request content into a Comment object
                $dto = $this->serializer->deserialize(
                     $request->getContent(),
                    $this->getObjectFromType($type),
                    'xml'
                );
                $request->attributes->set('dto', $dto);
            }
            // set the comment attribute on the request,
            // this will be used as the $comment argument of the controller
        }
        catch (XmlErrorException $e)
        {
            // we've tried, but it didn't work out
            return;
        }
        catch (\Exception $e) {
            return;
        }
    }
    
    private function getObjectFromType($type)
    {
        return 'AppBundle\\DataTransferObject\\' . ucfirst($type);
    }
}