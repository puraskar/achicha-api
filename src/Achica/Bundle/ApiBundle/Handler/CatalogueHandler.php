<?php
/**
 * Catalogue Handler for Catalogue operations
 *
 * @author Puraskar Sapkota <puraskar.sapkota@worldstores.co.uk>
 * @package api
 * @category handler
 * @since 2015.12.22
 */

namespace Achica\Bundle\ApiBundle\Handler;


use AppBundle\Entity\Catalogue;
use Achica\Bundle\ApiBundle\Form\CatalogueType;
use Achica\Bundle\ApiBundle\Request\Query;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Achica\Bundle\ApiBundle\Handler\AbstractHandler;
use Achica\Bundle\ApiBundle\Handler\ApiHandlerInterface;
use AppBundle\DataTransferObject\DTOInterface;

class CatalogueHandler extends AbstractHandler implements ApiHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getList(Request $request)
    {
        return $this->handleList(
            $this->repository->createQueryBuilder('catalogue'),
            new Query($request->query)
        );
    }

    /**
     * @param integer $id
     * @return object
     */
    public function getOneById($id)
    {
        $entity = $this->repository->find($id);

        if (!$entity instanceof Catalogue) {
            throw new NotFoundHttpException('Catalogue not found');
        }

        return $entity;
    }

    /**
     * @param string $field
     * @param string $value
     * @return Catalogue
     */
    public function getOneBy($field, $value)
    {
        $entity = $this->repository->findOneBy([$field => $value]);

        if (!$entity instanceof Catalogue) {
            throw new NotFoundHttpException('Catalogue not found');
        }

        return $entity;
    }

    /**
     * @param Request $request
     * @param DTOInterface $dto
     * @return mixed
     */
    public function post(Request $request, DTOInterface $dto)
    {
        $catalogue = $this->dtoToEntityTransformerFactory->getTransformer('catalogue')->transform($dto);
        return $this->handlePost($catalogue);
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return mixed
     */
    public function patch($id, Request $request, DTOInterface $dto)
    {
        $entity = $this->getOneById($id);
        $catalogue = $this->dtoToEntityTransformerFactory->getTransformer('catalogue')->transform($dto, $entity);
        return $this->handlePost($catalogue);
    }

    /**
     * @param integer $id
     * @param Request $request
     * @return HandlerResponse
     */
    public function put($id, Request $request, DTOInterface $dto)
    {
        $handlerResponse = new HandlerResponse();
        $handlerResponse->updating();
        $entity = $this->repository->find($id);
        if ($entity === null) {
            $entity = new Catalogue();
            $handlerResponse->creating();
        }
        $catalogue = $this->dtoToEntityTransformerFactory->getTransformer('catalogue')->transform($dto, $entity);
        $handlerResponse->entity = $catalogue;
        return $handlerResponse;
    }

    /**
     * @param integer $id
     * @return void
     */
    public function delete($id)
    {
        throw new \BadMethodCallException(sprintf('Method "%s" is not allowed', __METHOD__));

    }
}