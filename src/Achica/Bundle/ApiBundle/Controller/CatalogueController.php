<?php
/**
 * Catalogue Controller for Catalogue operations
 *
 * @author Puraskar Sapkota <puraskar.sapkota@worldstores.co.uk>
 * @package api
 * @category controller
 * @since 2015.12.22
 */

namespace Achica\Bundle\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\View;
use AppBundle\Exception\InvalidFormException;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\DataTransferObject\DTOInterface;

/**
 * Catalogue controller.
 *
 */
class CatalogueController extends RestController
{

    /**
     * Lists all Catalogue entities.
     *
     * @ApiDoc(
     *      resource=true,
     *      description="GET a list of Catalogue",
     *      section="Catalogue",
     *      filters={
     *          {"name"="_sort", "dataType"="string"},
     *          {"name"="_limit", "dataType"="integer", "default"=10},
     *          {"name"="_page", "dataType"="integer", "default"=1},
     *      }
     * )
     * @View(serializerEnableMaxDepthChecks=true)
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function getListAction(Request $request)
    {
        $handler = $this->get('achica_api.catalogue_handler');

        return $this->view(
            $handler->getList($request),
            Codes::HTTP_OK
        );
    }

    /**
     * Returns a single Catalogue for the supplied ID.
     *
    * @ApiDoc(
    *      resource=true,
    *      description="GET a single Catalogue by ID",
    *      section="Catalogue",
    *      requirements={
    *          {"name"="id", "dataType"="integer", "required"=true, "description"="ID of Catalogue"}
    *      },
    *      statusCodes={
    *          200="Returned when successful",
    *          404="Returned when the Catalogue is not found"
    *      }
    * )
     */
    public function getOneByIdAction($id)
    {
        $handler = $this->get('achica_api.catalogue_handler');
        $entity = $handler->getOneById($id);

        return $this->view(
            $entity,
            Codes::HTTP_OK
        );
    }
    /**
     * Creates a new Catalogue entity.
     *
    * @ApiDoc(
    *      resource = true,
    *      section="Catalogue",
    *      description="Create an Catalogue",
    *      input="Achica\Bundle\ApiBundle\Form\CatalogueType",
    *      statusCodes={
    *          201="Catalogue created successfully",
    *          400="Bad request. Validation error"
    *      }
    * )
    * @param Request $request
    * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request, DTOInterface $dto)
    {
        try {
            $handler = $this->get('achica_api.catalogue_handler');
            $entity = $handler->post($request, $dto);

            return $this->routeRedirectView('feeds_catalogue_id', ['id' => $entity->getId()]);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }

    }


    /**
    * Updates an existing AppBundle\Entity\Catalogue entity.
    *
    * NOTE: When performing an update, it is a full update.
    * This means that only the fields supplied in the JSON body will be set.
    * All other fields will be reset/set to NULL.
    * If you want to do a partial update, use the PATCH endpoint below.
    *
    * @ApiDoc(
    *      resource=true,
    *      section="Catalogue",
    *      description="Update a Catalogue or create it if it doesn't exist",
    *      resourceDescription="Operations on Catalogue.",
    *      input="Achica\Bundle\ApiBundle\Form\CatalogueType",
    *      statusCodes={
    *          201="Catalogue created successfully.",
    *          204="Catalogue updated successfully.",
    *          400="Bad request. Validation error.",
    *          404="Catalogue not found."
    *      }
    * )
    * @param integer $id
    * @param Request $request
    * @return \FOS\RestBundle\View\View
    */
    public function putAction($id, Request $request)
    {
        try {
            $handler = $this->get('achica_api.catalogue_handler');

            /** @var \ApiBundle\Handler\HandlerResponse $response */
            $response = $handler->put($id, $request);

            $statusCode = $response->isCreate() ? Codes::HTTP_CREATED : Codes::HTTP_NO_CONTENT;

            return $this->routeRedirectView('feeds_catalogue_id', ['id' => $response->entity->getId()], $statusCode);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }
    /**
     * Updates an existing AppBundle\Entity\Catalogue entity.
     *
    * @ApiDoc(
    *      resource = true,
    *      section="Catalogue",
    *      description="Update an Catalogue",
    *      input="Achica\Bundle\ApiBundle\Form\CatalogueType",
    *      statusCodes={
    *          204="Catalogue updated successfully",
    *          400="Bad request. Validation error",
    *          404="Catalogue not found. ID is missing"
    *      }
    * )
    * @param integer $id
    * @param Request $request
    * @return \FOS\RestBundle\View\View
     */
    public function patchAction($id, Request $request)
    {
        try {
            $handler = $this->get('achica_api.catalogue_handler');
            $entity = $handler->patch($id, $request);

            return $this->routeRedirectView('feeds_catalogue_id', ['id' => $entity->getId()], Codes::HTTP_NO_CONTENT);
        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }
}
