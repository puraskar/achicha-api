<?php

namespace Achica\Bundle\ApiBundle\View;


class Error 
{
    public $code;

    public $message;

    public $errors;
}