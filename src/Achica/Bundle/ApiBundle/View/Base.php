<?php

namespace Achica\Bundle\ApiBundle\View;


class Base 
{
    const API_VERSION = "1.0.0";

    /**
     * @var string
     */
    public $apiVersion = self::API_VERSION;

    /**
     * @var Data
     */
    public $data;

    /**
     * @var Error
     */
    public $error;
}