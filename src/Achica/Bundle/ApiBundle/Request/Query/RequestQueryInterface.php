<?php


namespace Achica\Bundle\ApiBundle\Request\Query;


interface RequestQueryInterface
{
    /**
     * @param mixed $value
     * @param string $queryKey
     */
    public function __construct($value, $queryKey);

    /**
     * @return string
     */
    public function __toString();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return string
     */
    public function getQueryKey();
}