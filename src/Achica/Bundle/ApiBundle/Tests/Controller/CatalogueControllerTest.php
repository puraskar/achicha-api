<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CatalogueControllerTest extends WebTestCase
{
    public function testPostCatalogue()
    {
        $catalogue = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<catalogue>
    <date><![CDATA[%now%]]></date>
    <clientId>3</clientId>
</catalogue>
EOF;

        $catalogue = str_replace('%now%', '2015-10-10', $catalogue);

        $server = array(
            'HTTP_ACCEPT' => 'text/xml',
            'HTTP_CONTENT_TYPE' => 'text/xml; charset=UTF-8',
        );

        $client = $this->createClient();
        $response = $client->request(
            'POST', '/feeds/catalogue?type=catalogue', 
            array(), array(), 
            $server, $catalogue
        );
        print_r($response); 
    }
}

