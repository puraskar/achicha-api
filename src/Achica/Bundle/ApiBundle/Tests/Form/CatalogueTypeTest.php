<?php
/**
 * CatalogueType Test
 *
 * @author Puraskar Sapkota <puraskar.sapkota@worldstores.co.uk>
* @package api
* @category test
* @since 2015.12.22
*/

namespace Achica\Bundle\ApiBundle\Tests\Form;

use Achica\Bundle\ApiBundle\Form\CatalogueType;
use AppBundle\Entity\Catalogue;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\Forms;
use Nelmio\ApiDocBundle\Form\Extension\DescriptionFormTypeExtension;

class CatalogueTypeTest extends TypeTestCase
{
    protected function setUp()
    {
        parent::setUp();

        $this->factory = Forms::createFormFactoryBuilder()
            ->addExtensions($this->getExtensions())
            ->addTypeExtension(
                new DescriptionFormTypeExtension()
            )
            ->getFormFactory();

        $this->builder->setFormFactory($this->factory);
    }

    public function testSubmitValidData()
    {
        $formData = [
            'date' => "2015-12-22T16:39:30+00:00",
            'clientId' => ,
        ];

        $type = new CatalogueType();
        $form = $this->factory->create($type);

        $object = new Catalogue();
        $object->setDate("2015-12-22T16:39:30+00:00");
        $object->setClientId();

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}