Feature:
    Manage Order Data

    Background:
        Given that I have a client "CLIENT-ACHICA"
        AND the clients key is "ACHICA-KEY"

    Scenario: Add a New Order with valid Content
        When I send a POST request to "/feeds/CLIENT-ACHICA/NO/ACHICA-KEY" with body:
            """
                <?xml version="1.0" encoding="UTF-8"?>
                <order>
                    <orderId>order-id-1</orderId>
                    <clientId>c-id-1</clientId>
                    <catalogueId>cat-id-1</catalogueId>
                    <doNotShip>1</doNotShip>
                    <originalQty>10</originalQty>
                    <origin>WEB</origin>
                    <language>en</language>
                    <orderDate>2015-12-15T13:15:30Z</orderDate>
                    <customerNote>customer note</customerNote>
                    <deliveryInstruction> Some instruction </deliveryInstruction>
                    <shippingMethod> STANDARD </shippingMethod>
                    <shippingCost unit="GBP">20</shippingCost>
                    <shippingCostNet>20.05</shippingCostNet>
                    <shippingTax rate="10.20">20.05</shippingTax>
                    <giftWrap>1</giftWrap>
                    <subtotal unit="GBP">200.00</subtotal>
                    <taxes  unit="GBP" amount="25.00">
                        <tax type="VAT">25.00</tax>
                    </taxes>
                    <totalPrice unit="GBP">225.00</totalPrice>
                    <billToAddress>
                        <title> bill to title </title>
                        <companyName> bill to company </companyName>
                        <department> dept </department>
                        <firstName> first name </firstName>
                        <lastName> last name </lastName>
                        <address1> Address 1 </address1>
                        <address2> </address2>
                        <city> London </city>
                        <state> State? </city>
                        <country> UK </city>
                        <phone> 077144483833939 </phone>
                        <fax> 08766556677 </fax>
                        <mobile></mobile>
                        <email>sysdev@worldstores.co.uk</email>
                        <type>CC</type>
                    </billToAddress>
                    <shipToAddress>
                        <title> ship to title </title>
                        <companyName> ship to company </companyName>
                        <department> dept </department>
                        <firstName> first name </firstName>
                        <lastName> last name </lastName>
                        <address1> Address 1 </address1>
                        <address2> </address2>
                        <city> London </city>
                        <state> State? </city>
                        <country> UK </city>
                        <phone> 077144483833939 </phone>
                        <fax> 08766556677 </fax>
                        <mobile></mobile>
                        <email>sysdev@worldstores.co.uk</email>
                    </shipToAddress>
                    <payments type="CC">
                        <payment>
                            <tender>VISA</tender>
                        </payment>
                    </payments>
                    <lines>
                        <line>
                            <lineNumber>1</lineNumber>
                            <sku>sku-1</sku>
                            <altSku>alt-sku-1</sku>
                            <eanCode>1234567890987</eanCode>
                            <upcCode>345678909876</upcCode>
                            <name> line-name </name>
                            <weight>5.0</weight>
                            <height>46.5</height>
                            <length>77</length>
                            <width>23.5</width>
                            <cost>25.0</cost>
                            <price>50.0</price>
                            <customsNumber>cust113</customsNumber>
                            <countryOfOrigin>ES</countryOfOrigin>
                            <productType>P</productType?
                            <backorderable>1</backorderable>
                            <componentOf>
                                <sku>sku-1</sku>
                                <quantity>2</quantity>
                            </componentOf>
                            <contents>
                                <propertyName> name-1</propertyName>
                                <values>
                                    <value>
                                        <lngId>EN</lngId>
                                        <valueContent>Some text about Value Content</valueContent>
                                    </value>
                                </values>
                            </contents>
                        </line>
                    </lines>
                    <services>
                        <service>
                            <serviceCode>CUSTOMISATION_MESSAGE</serviceCode>
                            <serviceInstruction> some instruction </serviceInstruction>
                            <serviceProperties>
                                <property>
                                    <name> property-name </name>
                                </property>
                            </serviceProperties>
                            <serviceContent>
                                <content name="name-1"> some-content</content>
                            </serviceContent>
                            <description> some text of decription </description>
                            <serviceOriginalPrice unit="GBP">200.00</serviceOriginalPrice>
                            <serviceUnitPrice unit="GBP"> 100.00 </serviceUnitPrice>
                            <serviceTotalNet unit="GBP"> 300.00 </serviceTotalNet>
                            <serviceTotal unit="GBP"> 350.00 </serviceTotal>
                            <taxes unit="GBP" amount="150.0">
                                <tax type="VAT" rate="15">100</tax>
                            </taxes>
                        </service>
                    </services>
                    <orderDiscount>
                        <orderLineDiscount>
                            <fieldText>SUMMER15X0753</fieldText>
                            <value>20.50</value>
                        </orderLineDiscount>
                    </orderDiscount>
                    <distributionCentre> The great center </distributionCentre>
                    <originalPrice>200.00</originalPrice>
                    <messages>
                        <message type="ADVICE_NOTE"> Some advice note </message>
                    </messages>
                </order>
            """

        Then the response code should be 201
        And the response should be equal to
            """
                <?xml version="1.0" encoding="UTF-8"?>
                <apiResponse>
                    <token>token-for-order-id-1</token>
                    <success>true</success>
                    <feedType>NO</feedType>
                </apiResponse>
            """

    Scenario: Add a New Order with invalid Content
        Given that I want to add "new order"
        When I send a POST request to "/feeds/CLIENT-ACHICA/NO/ACHICA-KEY" with body:
            """
                <?xml version="1.0" encoding="UTF-8"?>
                <order>
                    <orderId>order-id-2</orderId>
                    <clientId>client-id-cant-be-more-than-10-characters</clientId>
                    <catalogueId>cat-id-2</catalogueId>
                    <doNotShip>1</doNotShip>
                    <originalQty>10</originalQty>
                    <origin>WEB</origin>
                </order>
            """

        Then the response code should be 400
        And the response should be equal to
            """
                <?xml version="1.0" encoding="UTF-8"?>
                <apiResponse>
                    <token>token-for-order-id-2</token>
                    <success >false</success >
                    <feedType>NO</feedType>
                    <errors>
                        <error>Invalid Client ID sent on URL</error>
                    </errors>
                </apiResponse>
            """